This resource pack uses a borderless "frosted" texture for all stained glass variants. The frosted glass variants are intended to provide a borderless/tileable glass effect without sacrificing visibility or detail.

![Various frosted glass textures](./.images/frosted_glass.png)

**Note:** Unfortunately, partial alpha is not possible in the regular glass texture, so frosted glass can only be applied to the stained glass blocks and panes.